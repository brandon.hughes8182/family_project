const express = require('express');
const FamilyController = require('./family.controller');

const FamilyRouter = new express.Router();

FamilyRouter.get('/', FamilyController.getFamily);
FamilyRouter.post('/', FamilyController.createMember);

module.exports = FamilyRouter;