const knex = require('../db');

async function getFamily(req, res) {
	const familyTable = await knex('family-members')
		.select('id', 'first_name', 'last_name', 'gender', 'age');

	return res.status(200).json(familyTable);
}

async function createMember(req, res) { // HTTP POST
    // Assign the new student's information in the request body to a variable.
    const newMember = req.body;
    // Insert the new student information into the database, retrieving the new id, first name,
    // last name, birth date, and enrolled date.

    //const timestamp = new Date();
    console.log(newMember.first_name);
    // This returns a single-length array containing the new record's id.
    const idArray = await knex('family-members').insert({
        first_name: newMember.first_name,
        last_name: newMember.last_name,
        gender: newMember.gender,
        age: newMember.age,
        //created_at: timestamp,
        //updated_at: timestamp,
    });
    
    const newId = idArray[0];
    const response = await knex('family-members')
        .select('id', 'first_name', 'last_name', 'gender', 'age')
        .where({ id: newId })
        .first()
    
    // Send the new student information, now with an ID, back to the client.
    return res.status(201).json(response);
    
}
module.exports = {
    getFamily,
    createMember
};