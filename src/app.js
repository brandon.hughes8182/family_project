const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const familyRouter = require('./Family/family.router');
const server = express()

server.use(cors());
server.use(express.json())

const PORT = 2000

server.get("/", (req, res) => {
	res.json({ message: "Host Folder" });
})

server.use('/family', familyRouter);

server.listen(PORT, () => {
	console.log(`\n*** Server running on port ${PORT} ***\n`);
});