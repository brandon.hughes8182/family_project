exports.seed = function (knex) {
	// Deletes ALL existing entries
	return knex('family-members').del()
		.then(function () {
			// Inserts seed entries
			return knex('family-members').insert([
				{
					first_name: 'Brandon',
					last_name: 'Hughes',
					gender: 'Male',
					age: 39
				},
				{
					first_name: 'Echo',
					last_name: 'Hughes',
					gender: 'Male',
					age: 10
                }
			]);
		});
};
