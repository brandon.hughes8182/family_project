const path = require('path');
// Update with your config settings.

module.exports = {
    development: {
        client: 'sqlite3',
        connection: {
            filename: path.join(__dirname, 'Family-Server.db3')
        },
        useNullAsDefault: true,
        migrations: {
            tableName: 'knex_migrations'
        }
    }
};
