exports.up = function (knex) {
	return knex.schema.createTable('family-members', function (table) {
		table.increments('id');
		table.text('first_name', 128)
			.notNullable();
		table.text('last_name', 128)
			.notNullable();
		table.text('gender', 10)
			.notNullable;
		table.integer('age', 3)
			.notNullable;
		table.timestamps(true, true);
	})
};

exports.down = function (knex) {
	return knex.schema.dropTable('family-members');
};
